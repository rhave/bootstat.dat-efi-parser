#!/usr/bin/env python3

#
# BOOTSTAT.DAT parser
# Rasmus Have, Derant ApS 2019
#

VERSION=0.2

import sys
import struct
import json

printjson = True

if len(sys.argv) != 3:
    print('BOOTSTAT.DAT parser v' + str(VERSION), file=sys.stderr)
    print('Usage: ' + sys.argv[0] + ' <outputtype> <BOOTSTAT.DAT-file>', file=sys.stderr)
    print(' where outputtype is either json or csv', file=sys.stderr)
    sys.exit(-1)

if sys.argv[1] == 'csv':
    printjson = False
    
jsonresult = {}

f = open(sys.argv[2], 'rb')

version, = struct.unpack('<i', f.read(4))
if (printjson):
    jsonresult['version'] = version
else:
    print('Version:', version)

header_size, = struct.unpack('<i', f.read(4))
if (printjson):
    jsonresult['header_size'] = header_size
else:
    print('Header size:', header_size)

file_size, = struct.unpack('<i', f.read(4))
if (printjson):
    jsonresult['file_size'] = file_size
else:
    print('File size:', file_size)

valid_data_size, = struct.unpack('<i', f.read(4))
if (printjson):
    jsonresult['valid_data_size'] = valid_data_size
else:
    print('Valid data size:', valid_data_size)

i = 0
while f.tell() < header_size:
    unknown_header_dword, = struct.unpack('<i', f.read(4))
    if (printjson):
        jsonresult['unknown_header_dword_' + str(i)] = unknown_header_dword
    else:
        print('Unknown header dword ' + str(i) + ':', unknown_header_dword)
    i += 1

if (not printjson):
    print('End of header')

jsonresult['events'] = []

while f.tell() < valid_data_size:
    start_position = f.tell()
    timestamp, = struct.unpack('<i', f.read(4))
    zero_field, = struct.unpack('<i', f.read(4))
    event_source_guid_0, = struct.unpack('>Q', f.read(8))
    event_source_guid_1, = struct.unpack('>Q', f.read(8))
    size_of_entry, = struct.unpack('<i', f.read(4))
    severity_code, = struct.unpack('<i', f.read(4))
    entry_version, = struct.unpack('<i', f.read(4))
    event_identifier, = struct.unpack('<i', f.read(4))
    if event_identifier == 1:
        event_year, = struct.unpack('<H', f.read(2))
        event_month, = struct.unpack('<H', f.read(2))
        event_day, = struct.unpack('<H', f.read(2))
        event_hour, = struct.unpack('<H', f.read(2))
        event_minute, = struct.unpack('<H', f.read(2))
        event_second, = struct.unpack('<H', f.read(2))
        event_zero, = struct.unpack('<H', f.read(2))
        event_seven, = struct.unpack('<H', f.read(2))
        event_one, = struct.unpack('<i', f.read(4))
        event_zero2, = struct.unpack('<i', f.read(4))
        if (printjson):
            jsonresult['events'].append({'event_name': "Log file initialised",
                'timestamp': timestamp,
                'zero_field': zero_field,
                'source_guid': '%0.2X' % event_source_guid_0 + '%0.2X' % event_source_guid_1,
                'size_of_entry': size_of_entry,
                'severity_code': severity_code,
                'entry_version': entry_version,
                'event_identifier': event_identifier,
                'event_time_struct': str(event_year) + '-' + '%02d' % event_month + '-' + '%02d' % event_day + ' ' + '%02d' % event_hour + ':' + '%02d' % event_minute + ':' + '%02d' % event_second,
                'event_zero_field_0': event_zero,
                'event_seven': event_seven,
                'event_one': event_one,
                'event_zero_field_1': event_zero2})
        else:
            print('event_name,timestamp,zero,source_guid,size_of_entry,severity_code,entry_version,event_identifier,event_time_struct,event_zero0,event_seven,event_one,event_zero1')
            print('"Log file initialised",' + str(timestamp) + ',' + str(zero_field) + ',' + '%0.2X' % event_source_guid_0 + '%0.2X' % event_source_guid_1 + ',' + str(size_of_entry) + ',' + str(severity_code) + ',' + str(entry_version) +',' + str(event_identifier) + ',' + str(event_year) + '-' + '%02d' % event_month + '-' + '%02d' % event_day + ' ' + '%02d' % event_hour + ':' + '%02d' % event_minute + ':' + '%02d' % event_second + ',' + str(event_zero) + ',' + str(event_seven) + ',' + str(event_one) + ',' + str(event_zero2))
    elif event_identifier == 17:
        event_app_guid_0, = struct.unpack('>Q', f.read(8))
        event_app_guid_1, = struct.unpack('>Q', f.read(8))
        event_type_of_start, = struct.unpack('<i', f.read(4))
        s = ''
        c = f.read(2)
        while struct.unpack('<H', c)[0] != 0:
            #print(len(s), struct.unpack('<H', c), str(c.decode('utf-16')))
            s += c.decode('utf-16')
            c = f.read(2)
        if (printjson):
            jsonresult['events'].append({'event_name': "Boot application launch",
                                         'timestamp': timestamp,
                                         'zero_field': zero_field,
                                         'source_guid': '%0.2X' % event_source_guid_0 + '%0.2X' % event_source_guid_1,
                                         'size_of_entry': size_of_entry,
                                         'severity_code': severity_code,
                                         'entry_version': entry_version,
                                         'event_identifier': event_identifier,
                                         'event_app_guid': '%0.2X' % event_app_guid_0 + '%0.2X' % event_app_guid_1,
                                         'event_type_of_start': event_type_of_start,
                                         'event_app_pathname': s})
        else:
            print('event_name,timestamp,zero,source_guid,size_of_entry,severity_code,entry_version,event_identifier,event_app_guid,event_type_of_start,event_app_pathname')
            print('"Boot application launched",' + str(timestamp) + ',' + str(zero_field) + ',' + '%0.2X' % event_source_guid_0 + '%0.2X' % event_source_guid_1 + ',' + str(size_of_entry) + ',' + str(severity_code) + ',' + str(entry_version) +',' + str(event_identifier) + ',' + '%0.2X' % event_app_guid_0 + '%0.2X' % event_app_guid_1 + ',' + str(event_type_of_start) + ',"' + s + '"')
    elif event_identifier == 18:
        event_app_guid_0, = struct.unpack('>Q', f.read(8))
        event_app_guid_1, = struct.unpack('>Q', f.read(8))
        event_type_of_return, = struct.unpack('<i', f.read(4))
        if (printjson):
            jsonresult['events'].append({'event_name': "Boot application return",
                                         'timestamp': timestamp,
                                         'zero_field': zero_field,
                                         'source_guid': '%0.2X' % event_source_guid_0 + '%0.2X' % event_source_guid_1,
                                         'size_of_entry': size_of_entry,
                                         'severity_code': severity_code,
                                         'entry_version': entry_version,
                                         'event_identifier': event_identifier,
                                         'event_app_guid': '%0.2X' % event_app_guid_0 + '%0.2X' % event_app_guid_1,
                                         'event_type_of_return': event_type_of_return})
        else:
            print('event_name,timestamp,zero,source_guid,size_of_entry,severity_code,entry_version,event_identifier,event_app_guid,event_type_of_return')
            print('"Boot application return",' + str(timestamp) + ',' + str(zero_field) + ',' + '%0.2X' % event_source_guid_0 + '%0.2X' % event_source_guid_1 + ',' + str(size_of_entry) + ',' + str(severity_code) + ',' + str(entry_version) +',' + str(event_identifier) + ',' + '%0.2X' % event_app_guid_0 + '%0.2X' % event_app_guid_1 + ',' + str(event_type_of_return))
    elif event_identifier == 50:
        event_value_0, = struct.unpack('>I', f.read(4))
        event_value_1, = struct.unpack('>I', f.read(4))
        event_value_2, = struct.unpack('>I', f.read(4))
        s = ''
        c = f.read(2)
        while struct.unpack('<H', c)[0] != 0:
            #print(len(s), struct.unpack('<H', c), str(c.decode('utf-16')))
            s += c.decode('utf-16')
            c = f.read(2)
        if (printjson):
            jsonresult['events'].append({'event_name': "Event 50 (0x32)",
                                         'timestamp': timestamp,
                                         'zero_field': zero_field,
                                         'source_guid': '%0.2X' % event_source_guid_0 + '%0.2X' % event_source_guid_1,
                                         'size_of_entry': size_of_entry,
                                         'severity_code': severity_code,
                                         'entry_version': entry_version,
                                         'event_identifier': event_identifier,
                                         'event_value_0': '%0.2X' % event_value_0,
                                         'event_value_1': '%0.2X' % event_value_1,
                                         'event_value_2': '%0.2X' % event_value_2,
                                         'event_app_pathname': s})
        else:
            print('event_name,timestamp,zero,source_guid,size_of_entry,severity_code,entry_version,event_identifier,event_value_0,event_value_1,event_value_2,event_app_pathname')
            print('"Event 50 (0x32)",' + str(timestamp) + ',' + str(zero_field) + ',' + '%0.2X' % event_source_guid_0 + '%0.2X' % event_source_guid_1 + ',' + str(size_of_entry) + ',' + str(severity_code) + ',' + str(entry_version) +',' + str(event_identifier) + ',' + ('%0.2X' % event_value_0) + ',' + ('%0.2X' % event_value_1) + ',' + ('%0.2X' % event_value_2) + ',"' + s + '"')
    elif event_identifier == 83:
        event_value_0, = struct.unpack('>I', f.read(4))
        event_value_1, = struct.unpack('>I', f.read(4))
        event_value_2, = struct.unpack('>I', f.read(4))
        if (printjson):
            jsonresult['events'].append({'event_name': "Event 83 (0x53)",
                                         'timestamp': timestamp,
                                         'zero_field': zero_field,
                                         'source_guid': '%0.2X' % event_source_guid_0 + '%0.2X' % event_source_guid_1,
                                         'size_of_entry': size_of_entry,
                                         'severity_code': severity_code,
                                         'entry_version': entry_version,
                                         'event_identifier': event_identifier,
                                         'event_value_0': '%0.2X' % event_value_0,
                                         'event_value_1': '%0.2X' % event_value_1,
                                         'event_value_2': '%0.2X' % event_value_2})
        else:
            print('event_name,timestamp,zero,source_guid,size_of_entry,severity_code,entry_version,event_identifier,event_value_0,event_value_1,event_value_2')
            print('"Event 83 (0x53)",' + str(timestamp) + ',' + str(zero_field) + ',' + '%0.2X' % event_source_guid_0 + '%0.2X' % event_source_guid_1 + ',' + str(size_of_entry) + ',' + str(severity_code) + ',' + str(entry_version) +',' + str(event_identifier) + ',' + ('%0.2X' % event_value_0) + ',' + ('%0.2X' % event_value_1) + ',' + ('%0.2X' % event_value_2))
    else:
        if (printjson):
            event_values = []
            print(f.tell())
            print(start_position)
            print(size_of_entry)
            while f.tell() < start_position + size_of_entry:
                event_value_x, = struct.unpack('>I', f.read(4))
                event_values.append(event_value_x)
            jsonresult['events'].append({'event_name': "Event %d (0x%x)" % (event_identifier, event_identifier),
                                         'timestamp': timestamp,
                                         'zero_field': zero_field,
                                         'source_guid': '%0.2X' % event_source_guid_0 + '%0.2X' % event_source_guid_1,
                                         'size_of_entry': size_of_entry,
                                         'severity_code': severity_code,
                                         'entry_version': entry_version,
                                         'event_identifier': event_identifier,
                                         'event_values': event_values})
        else:
            print('Unknown event identifier:', event_identifier, "at position", start_position)
            i = 0
            while f.tell() < start_position + size_of_entry:
                unknown_entry_byte, = struct.unpack('B', f.read(1))
                print('Unknown entry byte ' + str(i) + ':', unknown_entry_byte)
                i += 1

if printjson:
    print(json.dumps(jsonresult))
